FROM node:18-alpine
LABEL name="Nguyen Quoc Huy"
LABEL email="huynq.bk1devopsonline230102@student.bkacad.edu.vn"
WORKDIR /app
COPY . .
EXPOSE 8080
ENV CLASS=SHB_DevOps APP_PORT=8080 APP_ENV=dev
RUN yarn install --production
CMD ["node", "src/index.js"]